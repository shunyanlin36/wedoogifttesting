package wedoogift.test.level.level1.modele;

import java.io.Serializable;

/**
 * Entite utilisateur
 *
 */
public class User implements Serializable {
	
	private static final long serialVersionUID = 5730930195812761488L;

	private int id;
	
	private int balance;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	public void addbalance(int amount) {
		this.balance = this.balance + amount;
	}
}
