package wedoogift.test.level.level1.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import wedoogift.test.level.level1.modele.Level1;
import wedoogift.test.level.level1.service.GiftService;

@RestController
public class GiftController {
	
	@Autowired
	GiftService giftService;
	
	/**
	 * Distribution carte cadeau
	 * @param level 
	 * 			-companies data Compagnie
	 * 			-users data user
	 * 			-distributions data distribution 
	 * @return level
	 * 			data calcul
	 */
	@GetMapping("/distributionGift")
	public Level1 distributionGift(@RequestBody Level1 level){
		return giftService.distributionGift(level);
	}
}
