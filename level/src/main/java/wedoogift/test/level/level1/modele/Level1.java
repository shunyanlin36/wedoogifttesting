package wedoogift.test.level.level1.modele;

import java.io.Serializable;
import java.util.List;

import wedoogift.test.level.level2.modele.Companie;


public class Level1 implements Serializable{
	
	private static final long serialVersionUID = 5068756558201498685L;
	
	private List<Companie> companies;
	
	private List<User> users;
	
	private List<Distribution> distributions;

	public List<Companie> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Companie> companies) {
		this.companies = companies;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Distribution> getDistributions() {
		return distributions;
	}

	public void setDistributions(List<Distribution> distributions) {
		this.distributions = distributions;
	}
	
	
}
