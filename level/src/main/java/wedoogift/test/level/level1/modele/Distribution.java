package wedoogift.test.level.level1.modele;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * Entite Distribution
 *
 */
public class Distribution implements Serializable{
	
	private static final long serialVersionUID = 4281411738931579548L;

		private int id;
		
		private int amount;
		
		private Date start_date;
		
		private Date end_date;
		
		private int company_id;
		
		private int user_id;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getAmount() {
			return amount;
		}
		public void setAmount(int amount) {
			this.amount = amount;
		}
		public Date getStart_date() {
			return start_date;
		}
		public void setStart_date(Date start_date) {
			this.start_date = start_date;
		}
		public Date getEnd_date() {
			return end_date;
		}
		public void setEnd_date(Date end_date) {
			this.end_date = end_date;
		}

		public int getCompany_id() {
			return company_id;
		}
		public void setCompany_id(int company_id) {
			this.company_id = company_id;
		}
		public int getUser_id() {
			return user_id;
		}
		public void setUser_id(int user_id) {
			this.user_id = user_id;
		}
}
