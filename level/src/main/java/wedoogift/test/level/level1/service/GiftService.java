package wedoogift.test.level.level1.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import wedoogift.test.level.level1.modele.Distribution;
import wedoogift.test.level.level1.modele.Level1;
import wedoogift.test.level.level1.modele.User;
import wedoogift.test.level.level2.modele.Companie;

@Service
public class GiftService {
	/**
	 * Fonction qui distribue les cadeaux
	 * @param level data level
	 * @return level apres calcul
	 */
	public Level1 distributionGift(Level1 level) {
		Level1 level1 = new Level1();
		List<Companie> companies = level.getCompanies();
		List<User> users = level.getUsers();
		List<Distribution> distributions = level.getDistributions();
		Companie companieTmp;
		User userTmp;
		if(distributions.isEmpty()) {
			return level;
		} else {
			Date dateNow = Calendar.getInstance().getTime();
			for (Distribution distribution : distributions) {
				if(distribution.getStart_date().compareTo(dateNow) < 0 &&
					distribution.getEnd_date().compareTo(dateNow) > 0 ) {
					companieTmp = companies.stream()
						.filter(companie -> distribution.getCompany_id() == companie.getId())
						.findFirst()
						.orElse(null);
					userTmp = users.stream()
						.filter(user -> distribution.getUser_id() == user.getId())
						.findFirst()
						.orElse(null);
					if(companieTmp != null && userTmp != null) {
						companieTmp.subBalance(distribution.getAmount());
						userTmp.addbalance(distribution.getAmount());
					}
				}
				
			}
		}
		level1.setCompanies(companies);
		level1.setUsers(users);
		level1.setDistributions(distributions);
		
		return level1;
	}

}
