package wedoogift.test.level.level1.modele;

import java.io.Serializable;

/**
 * Entite companie
 *
 */
public class Companie implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int id;
	
	private String name;
	
	private int balance;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	public void subBalance(int amount) {
		this.balance = this.balance - amount;
	}
	
}
