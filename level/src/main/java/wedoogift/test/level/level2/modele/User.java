package wedoogift.test.level.level2.modele;

import java.io.Serializable;
import java.util.List;

/**
 * Entite balance
 *
 */
public class User implements Serializable {
	
	private static final long serialVersionUID = 5730930195812761488L;

	private int id;
	
	private List<Balance> balance;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Balance> getBalance() {
		return balance;
	}

	public void setBalances(List<Balance> balance) {
		this.balance = balance;
	}

	
}
