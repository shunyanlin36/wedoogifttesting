package wedoogift.test.level.level2.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import wedoogift.test.level.level2.modele.Balance;
import wedoogift.test.level.level2.modele.Companie;
import wedoogift.test.level.level2.modele.Distribution;
import wedoogift.test.level.level2.modele.Level2;
import wedoogift.test.level.level2.modele.User;

@Service
public class RestaurantCardService {

	public Level2 distributionVouchers(Level2 level) {
		Level2 level2 = new Level2();
		List<Companie> companies = level.getCompanies();
		List<User> users = level.getUsers();
		List<Distribution> distributions = level.getDistributions();
		Companie companieTmp;
		User userTmp;
		Balance balanceTmp;
		if(distributions.isEmpty()) {
			return level;
		} else {
			Date dateNow = Calendar.getInstance().getTime();
			for (Distribution distribution : distributions) {
				if(distribution.getStart_date().compareTo(dateNow) < 0 &&
					distribution.getEnd_date().compareTo(dateNow) > 0 ) {
					companieTmp = companies.stream()
						.filter(companie -> distribution.getCompany_id() == companie.getId())
						.findFirst()
						.orElse(null);
					userTmp = users.stream()
						.filter(user -> distribution.getUser_id() == user.getId())
						.findFirst()
						.orElse(null);
					if(companieTmp != null && userTmp != null) {
						companieTmp.subBalance(distribution.getAmount());
						balanceTmp = userTmp.getBalance().stream()
								.filter(balance -> distribution.getWallet_id() == balance.getWallet_id())
								.findFirst()
								.orElse(null);
						balanceTmp.addAmount(distribution.getAmount());
					}
				}
			}
		}
		level2.setCompanies(companies);
		level2.setUsers(users);
		level2.setDistributions(distributions);
		
		return level2;
	}

}
