package wedoogift.test.level.level2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import wedoogift.test.level.level2.modele.Level2;
import wedoogift.test.level.level2.service.RestaurantCardService;

@RestController
public class RestaurantCardController {
	
	@Autowired
	RestaurantCardService restaurantService;
	
	/**
	 * Distribution carte restaurant
	 * @param level 
	 * 			-companies data Compagnie
	 * 			-users data user
	 * 			-distributions data distribution 
	 * @return level2
	 * 			data calcul
	 */
	@GetMapping("/distributionVouchers")
	public Level2 distributionGift(@RequestBody Level2 level){
		return restaurantService.distributionVouchers(level);
	}
}
