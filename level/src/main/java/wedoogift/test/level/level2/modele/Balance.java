package wedoogift.test.level.level2.modele;

import java.io.Serializable;

public class Balance implements Serializable {

	private static final long serialVersionUID = 1998599323698733621L;
	
	private int wallet_id;
	
	private int amount;

	public int getWallet_id() {
		return wallet_id;
	}

	public void setWallet_id(int wallet_id) {
		this.wallet_id = wallet_id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public void addAmount(int amount) {
		this.amount = this.amount + amount;
	}
	
}
